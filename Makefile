

all: compile

compile: realtime.c Tasks.c 
	gcc  realtime.c Tasks.c -o realtime -L /usr/local/lib -lrt -lpthread -lallegro_main -lallegro_image -lallegro -lallegro_primitives -lallegro_font
	
run: realtime
	sudo likwid-pin -c 0 ./realtime
	
clean: 
	rm realtime
