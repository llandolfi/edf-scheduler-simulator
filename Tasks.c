#include "Tasks.h"


//Function to add millisecond to a timespec
void time_add_ms(struct timespec* t, int ms){
	t->tv_sec += ms/1000;
	t->tv_nsec += (ms%1000)*1000000;
	if (t->tv_nsec > 1000000000) {
		t->tv_nsec -= 1000000000;
		t->tv_sec += 1;
	}
}

//Function to add 2 timespecs
void time_add (struct timespec* t, struct timespec* t1){
	t->tv_sec=t->tv_sec+t1->tv_sec;
	t->tv_nsec=t->tv_nsec+t1->tv_nsec;
	if (t->tv_nsec > 1000000000){
		t->tv_nsec -= 1000000000;
		t->tv_sec += 1;	}
}

//Function to subctract two timespecs
struct  timespec  tsSubtract (

#    if PROTOTYPES
		struct  timespec  time1,
		struct  timespec  time2)
#    else
time1, time2)

struct  timespec  time1 ;
struct  timespec  time2 ;
#    endif

{    /* Local variables. */
	struct  timespec  result ;



	/* Subtract the second time from the first. */

	if ((time1.tv_sec < time2.tv_sec) ||
			((time1.tv_sec == time2.tv_sec) &&
					(time1.tv_nsec <= time2.tv_nsec))) {		/* TIME1 <= TIME2? */
		result.tv_sec = result.tv_nsec = 0 ;
	} else {						/* TIME1 > TIME2 */
		result.tv_sec = time1.tv_sec - time2.tv_sec ;
		if (time1.tv_nsec < time2.tv_nsec) {
			result.tv_nsec = time1.tv_nsec + 1000000000L - time2.tv_nsec ;
			result.tv_sec-- ;				/* Borrow a second. */
		} else {
			result.tv_nsec = time1.tv_nsec - time2.tv_nsec ;
		}
	}

	return (result) ;

}

//Function to copy two timespec
void time_copy(struct timespec *td, struct timespec ts){
	td->tv_sec = ts.tv_sec;
	td->tv_nsec = ts.tv_nsec;}

//Function to compare two timespecs
int time_cmp(struct timespec t1, struct timespec t2){
	if (t1.tv_sec > t2.tv_sec) return 1;
	if (t1.tv_sec < t2.tv_sec) return -1;
	if (t1.tv_nsec > t2.tv_nsec) return 1;
	if (t1.tv_nsec < t2.tv_nsec) return -1; }

//Function used to set the period of a periodic task starting from the current time
void set_period(struct task_par *tp){
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_copy(&(tp->at), t);
	time_copy(&(tp->dl), t);
	time_add_ms(&(tp->at), tp->period);
	time_add_ms(&(tp->dl), tp->deadline);}

void *Stopper(){
	ALLEGRO_EVENT ev;
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	while (1){
		al_wait_for_event(event_queue, &ev);
		if (ev.keyboard.keycode==ALLEGRO_KEY_ESCAPE){al_rest(0);exit(0);}}

}



//Function implementing the execution of a generic periodic task
void *task(void *arg){

	int i; int j = 0; int loop; int rc; int redraw = 0;
	int oldAt;

	struct task_par *mytp;
	mytp = (struct task_par*)arg;

	//tp[i] corresponds to tid[i]
	i= mytp->arg;
	set_period(mytp);

	ALLEGRO_BITMAP * bitmap;
	ALLEGRO_BITMAP * clearbitmap;
	ALLEGRO_BITMAP * clearhalf;
	ALLEGRO_FONT *f1;



	al_init_primitives_addon();
	ALLEGRO_DISPLAY *display=mytp->display2;
	bitmap=al_create_bitmap(1,20);
	clearbitmap=al_create_bitmap(1280,66);

	/*the clearhalf bitmap is used to clear the second half of the display when the execution of the task crosses
	the display itself */
	clearhalf = al_create_bitmap(705,66);
	al_set_target_bitmap(bitmap);
	al_clear_to_color(al_map_rgb(20*i, 110*i, 110*i));
	al_set_target_bitmap(clearbitmap);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_set_target_bitmap(clearhalf);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_set_target_bitmap(al_get_backbuffer(display));

	f1 = al_create_builtin_font();
	if (!f1) {

		return 1;
	}

	char s[20];
	sprintf(s, "Periodic task %d", i);
	al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, (i)*70+6, 0, s);



	while (1) {
		//At the beginning: barrier to synchronize all the threds, including the scheduler
		if (j==0) {
			rc = pthread_barrier_wait(&barr);
			if ((rc != 0) && (rc != PTHREAD_BARRIER_SERIAL_THREAD)) {
				printf("Could not wait on barrier\n");
				exit(-1);}
			sem_wait(&sem_drow);
			al_draw_line(0+((mytp->period)/tick),(i+1)*70,0+((mytp->period)/tick),(i)*70+20,al_map_rgb(10,100,10),1);
			al_draw_filled_triangle(((mytp->period)/tick)-4,(i)*70+30,((mytp->period)/tick)+4,(i)*70+30,((mytp->period)/tick),(i)*70+20, al_map_rgb(10,100,10));
			al_draw_line(0+((mytp->deadline)/tick),(i+1)*70,0+((mytp->deadline)/tick),(i)*70+20,al_map_rgb(100,10,10),1);
			al_draw_filled_triangle(((mytp->deadline)/tick-4),(i+1)*70-10,((mytp->deadline)/tick+4),(i+1)*70-10,((mytp->deadline)/tick),(i+1)*70, al_map_rgb(100,10,10));
			al_flip_display();
			sem_post(&sem_drow);
			oldAt = (mytp->period)/tick;

		}

		//I copy my deadline to the shared array of task parameters in order to make the scheduler aware of my deadline
		time_copy(&tp[i].dl,(mytp->dl));

		if(j>0){
			sem_wait(&sem_drow);

			//If I'm startin to draw in the next display, I clear the second half of the previous display
			if(redraw == 1){
				al_draw_bitmap(clearhalf, 705, ((i ) * 70)+3 ,0);
				redraw = 0;
			}

			// If my next period will fall in the next display, I clear my part of the display
			if ((tempo+((mytp->period)/tick)) >1280){
				al_draw_bitmap(clearbitmap,0,((i)*70)+3,0);
				al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, (i)*70+6, 0, s);
				redraw = 1;
			}

			//I draw my next activation time and my deadline
			al_draw_line((oldAt+((mytp->period)/tick))%1280,(i+1)*70,(oldAt+((mytp->period)/tick))%1280,(i)*70+20,al_map_rgb(10,100,10),1);
			al_draw_filled_triangle((oldAt+((mytp->period)/tick)-4)%1280,(i)*70+30,(oldAt+((mytp->period)/tick)+4)%1280,(i)*70+30,(oldAt+((mytp->period)/tick))%1280,(i)*70+20, al_map_rgb(10,100,10));
			al_draw_line((oldAt+((mytp->deadline)/tick))%1280,(i+1)*70,(oldAt+((mytp->deadline)/tick))%1280,(i)*70+20,al_map_rgb(100,10,10),1);
			al_draw_filled_triangle((oldAt+((mytp->deadline)/tick-4))%1280,(i+1)*70-10,(oldAt+((mytp->deadline)/tick+4))%1280,(i+1)*70-10,(oldAt+((mytp->deadline)/tick))%1280,(i+1)*70, al_map_rgb(100,10,10));
			al_flip_display();
			sem_post(&sem_drow);
			oldAt = oldAt+((mytp->period)/tick); }


		//I wake up the scheduler and wait for its execution
		sem_post(&sem_from_scheduler);
		sem_wait(&sem_to_scheduler);

		// The next timespecs will be used to record the time of the beginning and the end of a iteration
		struct timespec t_curr,t_curr2;

		//For each iteration, i draw my bitmap at the current time, each iteration will last tick milliseconds
		for (loop=0; loop < mytp->iterations; loop++) {

			clock_gettime(CLOCK_MONOTONIC, &t_curr);

			sem_wait(&sem_drow);
			al_draw_bitmap(bitmap,tempo,((i+1)*70)-21,0);
			al_flip_display();

			sem_post(&sem_drow);

			time_add_ms(&t_curr,tick);
			clock_gettime(CLOCK_MONOTONIC, &t_curr2);

			//This while ensures that the iteration of the outer for will last at most tick millisecons
			while (time_cmp(t_curr2,t_curr)==-1){
				clock_gettime(CLOCK_MONOTONIC, &t_curr2);
			}

		}

		if (deadline_miss(mytp)){
			printf("!!! Task %d misssed its deadline!!!\n",i);}
		//pthread_setschedprio(tid[i],tick);
		wait_for_period(mytp);j++;}

}


//Scheduler Task: invoked before a thread starts to draw on the display, it simulates an EDF scheduler
void *Scheduler(){

	int loop=0; int Search_counter;

	/*The following array of task parameters is used in order to keep the relation between tp[i] and tid[i]
	since the scheduler will use an ordering algorithm to estabilish the threads priorities*/
	struct task_par copy[NT+9];


	while(1){
		//The scheduler waits to be awakened by one of the tasks
		sem_wait(&sem_from_scheduler);

		// I copy the task parameters in "copy"
		for(Search_counter=0; Search_counter <NT + 9; Search_counter++){
			copy[Search_counter] = tp[Search_counter];
		}

		// I sort tp according to the absolute deadline field
		sort(tp,1,NT+9);

		//The priorities are set according to the absolute deadline
		for(Search_counter=1; Search_counter < NT + 9; Search_counter++){
			pthread_setschedprio(tid[tp[Search_counter].arg],35-Search_counter-1);
		}
		for(Search_counter = 0; Search_counter < NT + 9; Search_counter++){
			tp[Search_counter]=copy[Search_counter];
		}

		// the scheduler wakes up the task that is waiting for him
		sem_post(&sem_to_scheduler);
		loop ++;}
}

/*Task executed by the timeadder thread, it increments the time variable tempo every tick milliseconds modulo
the size of the display. The thread that executes it has 99 as priority
 */
void *Vuoto(){
	struct timespec t_curr;
	int rc = pthread_barrier_wait(&barr);
	if ((rc != 0) && (rc != PTHREAD_BARRIER_SERIAL_THREAD)) {
		printf("Could not wait on barrier\n");
		exit(-1);}
	clock_gettime(CLOCK_MONOTONIC, &t_curr);
	while(1){
		tempo = (tempo + 1)%1280;
		time_add_ms(&t_curr, tick);
		clock_nanosleep(CLOCK_MONOTONIC,TIMER_ABSTIME, &(t_curr), NULL);}

}


void * Notificatore (){








}

// This is the task that emulates the total bandwidth server
void *Attivatore(){

	al_init_primitives_addon();
	int i; int count=0; int Button;
	struct coef_queue *mycol;
	ALLEGRO_DISPLAY *display=tp[1].display2;
	ALLEGRO_BITMAP * clearbitmap;
	ALLEGRO_FONT *f1;
	clearbitmap=al_create_bitmap(1280,66);
	al_set_target_bitmap(clearbitmap);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_set_target_bitmap(al_get_backbuffer(display));
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	struct timespec oldDeadline;
	struct timespec Deadline;
	struct timespec CurrentTime;
	struct timespec TimetoAdd;
	struct timespec interval;
	int Reldeadline;
	struct coef_queue myCol;

	f1 = al_create_builtin_font();
	if (!f1) {

		return 1;
	}

	char s[20];
	sprintf(s, " Total Bandwidth server ");
	al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, (NT)*70+6, 0, s);

	//At the beginning, the server calculates its coefficient
	double Server_coef = 0.0;
	for (i=1; i<NT; i++){
		Server_coef = Server_coef + ((double) tp[i].iterations * tick/ (double) tp[i].period);
	}

	/*In order to be sure, the server coefficient is computed as 0.95 minus the sum of
	 * the coefficient of the other periodic task*/
	Server_coef = 0.95 - Server_coef;
	ALLEGRO_EVENT ev;

	/*The server waits for a key from 1 to 9 to be pressed, as soon as one of this key is pressed, the
	 * corresponding task is executed. The duration of a sporadic task is proportional to its id */
	while(1){

		al_wait_for_event(event_queue, &ev);
		clock_gettime(CLOCK_MONOTONIC,&CurrentTime);
		//printf("Esegue At\n");

		/*First of all, it is determined if the sporadic task deadline must be computed starting
		  from the current time or from the previously computed deadline */

		if(time_cmp(CurrentTime,oldDeadline) == -1){
			TimetoAdd=oldDeadline;
		}

		if(time_cmp(oldDeadline,CurrentTime) == -1){
			TimetoAdd=CurrentTime;}



		if(ev.type==ALLEGRO_EVENT_KEY_DOWN){

			if (ev.keyboard.keycode == ALLEGRO_KEY_1 || ev.keyboard.keycode == ALLEGRO_KEY_2 || ev.keyboard.keycode == ALLEGRO_KEY_3
					|| ev.keyboard.keycode == ALLEGRO_KEY_4 || ev.keyboard.keycode == ALLEGRO_KEY_5 || ev.keyboard.keycode == ALLEGRO_KEY_6
					|| ev.keyboard.keycode == ALLEGRO_KEY_7 || ev.keyboard.keycode == ALLEGRO_KEY_8 || ev.keyboard.keycode == ALLEGRO_KEY_9){


				count = count +1;;



				if (ev.keyboard.keycode == ALLEGRO_KEY_1){
					Button = 1;

				}

				// the code for the activation of sporadic task 1 is in pratice repeated
				if (ev.keyboard.keycode==ALLEGRO_KEY_2){
					Button = 2;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_3){
					Button = 3;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_4){
					Button = 4;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_5){
					Button = 5;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_6){
					Button = 6;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_7){
					Button = 7;

				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_8){
					Button = 8;
				}
				if (ev.keyboard.keycode==ALLEGRO_KEY_9){
					Button = 9;

				}

				if(count > 3 && EmptyQueue()==1){
						count = 0;
						sem_wait(&sem_drow);
						al_draw_bitmap(clearbitmap, 0, (NT)*70+3, 0);
						al_draw_bitmap(clearbitmap, 0, (NT+1)*70+4, 0);
						al_draw_textf(f1, al_map_rgb(255, 255, 0), 10, (NT)*70+6, 0, s);
						al_flip_display();
						sem_post(&sem_drow);}
				//	sem_wait(&semN);

				/*The deadline of the sporadic task is calculated according to the server coefficient and the id of
						 the task */
				Deadline=calculate_deadline(Button,TimetoAdd,Server_coef);
				oldDeadline=Deadline;
				//time_copy(&tp[NT+Button-1].dl,Deadline);
				pthread_setschedprio(tid[tp[NT+Button-1].arg],45);
				interval =tsSubtract(Deadline,CurrentTime);

				//The relative deadline is computed (in milliseconds) in order to have a correct drawing
				Reldeadline=(int)interval.tv_sec*1000;
				Reldeadline=Reldeadline+(int)((interval.tv_nsec)/1000000);

				myColors[Button-1][indexE[Button-1]].colcoef1=rand()%255;
				myColors[Button-1][indexE[Button-1]].colcoef2=rand()%255;
				myColors[Button-1][indexE[Button-1]].colcoef3=rand()%255;
				myColors[Button-1][indexE[Button-1]].deadline = Deadline;
				myCol.colcoef1 = myColors[Button-1][indexE[Button-1]].colcoef1;
				myCol.colcoef2 = myColors[Button-1][indexE[Button-1]].colcoef2;
				myCol.colcoef3 = myColors[Button-1][indexE[Button-1]].colcoef3;
				//time_copy(&(myColors[Button-1][indexE[Button-1]].deadline) , Deadline);
				//myCol.deadline = Deadline;
				indexE[Button-1] ++;


				//printCol();
				//sem_post(&semN);

				sem_wait(&sem_drow);
				int temp=tempo;
				al_draw_line(temp,(NT+2)*70,temp,(NT+1)*70+5,al_map_rgb(myCol.colcoef1,myCol.colcoef2,myCol.colcoef3),1);
				al_draw_filled_triangle(temp-4,(NT+2)*70-50,temp+4,(NT+2)*70-50,temp,(NT+1)*70+4,al_map_rgb(myCol.colcoef1,myCol.colcoef2,myCol.colcoef3));
				al_flip_display();


				//There cannot be more than 3 instance of a sporadic task present in the display


				//The deadline is drawed
				int tdd = (temp+((Reldeadline)/tick))%1280;
				al_draw_line(tdd,(NT+1)*70-21,tdd,(NT)*70+20,al_map_rgb(myCol.colcoef1,myCol.colcoef2,myCol.colcoef3),1);
				al_draw_filled_triangle(tdd-4,(NT+1)*70-20-10,tdd+4,(NT+1)*70-20-10,tdd,(NT+1)*70-1-20,al_map_rgb(myCol.colcoef1,myCol.colcoef2,myCol.colcoef3));
				al_flip_display();
				sem_post(&sem_drow);
				//sem_post(&sem_from_scheduler);
				//sem_wait(&sem_to_scheduler);

				// the server wakes up the desired task
				sem_post(&sem_to_At[Button-1]);

				// the server waits for the termination of the invoked task, in order to do not overlap its deadline
				//sem_wait(&sem_from_At);

			}
		}
	}
}


// Function implementing the generic sporadic task
void *Aperiodic_task(void *arg){
	int i; int loop; int rc;
	struct task_par *mytp;
	mytp = (struct task_par*)arg;
	i= mytp->arg;
	struct timespec t_curr,t_curr2;
	int Reldeadline;
	ALLEGRO_BITMAP * bitmap;
	al_init_primitives_addon();
	ALLEGRO_DISPLAY *display=tp[1].display2;
	bitmap=al_create_bitmap(1,20);
	struct timespec Deadline;
	int colcoef1,colcoef2,colcoef3;
	rc = pthread_barrier_wait(&barr);
	if ((rc != 0) && (rc != PTHREAD_BARRIER_SERIAL_THREAD)) {
		printf("Could not wait on barrier\n");
		exit(-1);}


	while(1){
		//struct coef_queue *myCol;


		//every task sleeps waiting on its semaphore
		sem_wait(&sem_to_At[i-NT]);


		colcoef1 = myColors[i-NT][indexS[i-NT]].colcoef1;
		colcoef2 = myColors[i-NT][indexS[i-NT]].colcoef2;
		colcoef3 = myColors[i-NT][indexS[i-NT]].colcoef3;
		Deadline = myColors[i-NT][indexS[i-NT]].deadline;
		(indexS[i-NT]++);

		time_copy(&tp[i].dl,Deadline);
		printf("My deadline is %d.%ld\n", Deadline.tv_sec, Deadline.tv_nsec);


		sem_post(&sem_from_scheduler);
		sem_wait(&sem_to_scheduler);

		al_set_target_bitmap(bitmap);
		al_clear_to_color(al_map_rgb(colcoef1,colcoef2,colcoef3));
		al_set_target_bitmap(al_get_backbuffer(display));

		// each task stays in execution for a duration proportional to its id
		for (loop=0; loop < ((i-NT)+1)*10; loop++) {

			clock_gettime(CLOCK_MONOTONIC, &t_curr);

			sem_wait(&sem_drow);
			al_draw_bitmap(bitmap,tempo%1280,((NT+1)*70)-20-1,0);
			al_flip_display();
			sem_post(&sem_drow);

			time_add_ms(&t_curr,tick);
			clock_gettime(CLOCK_MONOTONIC, &t_curr2);
			while (time_cmp(t_curr2,t_curr)==-1){
				clock_gettime(CLOCK_MONOTONIC, &t_curr2);
			}

		}

		//at the end of its execution, the server is waked up
		//sem_post(&sem_from_At);

	}
}



// Function used to determine if the periodic task has missed its deadline
int deadline_miss(struct task_par *tp){
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	if (time_cmp(now, tp->dl) > 0) {
		tp->dmiss++;
		return 1;}
	return 0;}

// Function used by the periodic tasks to wait the correct amount of time
void wait_for_period(struct task_par *tp){
	clock_nanosleep(CLOCK_MONOTONIC,TIMER_ABSTIME, &(tp->at), NULL);
	time_add_ms(&(tp->at), tp->period);
	time_add_ms(&(tp->dl), tp->period);
}

// This the sorting function, the algorithm used to sort the threads according to their relative deadlines is the quicksort
void sort(struct task_par *array, int begin, int end) {
	struct timespec pivot;
	int l, r;
	if (end > begin) {
		time_copy(&pivot , array[begin].dl);
		l = begin + 1;
		r = end;
		while(l < r)
			if (time_cmp(array[l].dl,pivot)==-1)  //array[l] < pivot)
				l++;
			else {
				r--;
				swap(&array[l], &array[r]);
			}
		l--;
		swap(&array[begin], &array[l]);
		sort(array, begin, l);
		sort(array, r, end);
	}
}

// Function to swap the deadlines of two task parameters
void swap(struct task_par *a, struct task_par *b){
	struct task_par temp;
	temp.arg=b->arg;time_copy(&(temp.dl),b->dl);
	b->arg=a->arg;time_copy(&(b->dl),a->dl);
	a->arg=temp.arg;time_copy(&(a->dl), temp.dl);
}


//Function to calculate the deadline according to the total bandwidth server formula
struct timespec calculate_deadline(int id,struct timespec begin, double coef){
	time_add_ms(&begin,tick*((id*10)/coef));
	return begin;
}

int EmptyQueue(){
	int empty = 1;
	int i;
	for (i=0; i<9; i++){
		if (indexS[i] != indexE[i]){
			empty = 0;
		}
	}
	return empty;
}








